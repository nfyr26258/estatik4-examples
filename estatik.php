<?php

/**
 * Plugin name: Estatik 4 Code examples
 * Version: 0.0.1
 * Author: Estatik
 * Author URI: https://estatik.net
 * Description Estatik 4 code examples plugin.
 */

define( 'ESC_FILE', __FILE__ );
define( 'ESC_PLUGIN_PATH', dirname( ESC_FILE ) );

add_action( 'plugins_loaded', function() {

	if ( function_exists( 'es_get_the_property' ) ) {

		/**
		 * Create property custom fields in default and custom sections.
		 */
		include ESC_PLUGIN_PATH . '/includes/property-custom-field-and-section.php';

		/**
		 * Create property custom taxonomy field and add it to the Data Manager page.
		 */
		include ESC_PLUGIN_PATH . '/includes/property-custom-taxonomy-field-with-data-manager.php';

		/**
		 * Add custom metadata after save property.
		 */
		include ESC_PLUGIN_PATH . '/includes/add-custom-meta-after-save-property.php';

		/**
		 * Add custom section to estatik settings and add custom settings there.
		 */
		include ESC_PLUGIN_PATH . '/includes/custom-plugin-settings.php';

		/**
		 * Alter WP Query for [es_my_listing] shortcode example.
		 */
		include ESC_PLUGIN_PATH . '/includes/alter-es-my-listing-shortcode-wp-query-args.php';

		/**
		 * Example if es_get_properties_query_args function usage.
		 */
		include ESC_PLUGIN_PATH . '/includes/get-properties-func.php';

		/**
		 * Helper plugin functions for formatting strings and loading templates.
		 */
		include ESC_PLUGIN_PATH . '/includes/helper-func.php';

		/**
		 * Estatik framework usage.
		 */
		include ESC_PLUGIN_PATH . '/includes/framework-usage.php';

		/**
		 * Estatik framework usage.
		 */
		include ESC_PLUGIN_PATH . '/includes/create-custom-office-entity.php';

		/**
		 * Create custom section on agent and property single property page with custom content.
		 */
		include ESC_PLUGIN_PATH . '/includes/agent-and-property-custom-section-with-custom-content.php';
	}
} );
