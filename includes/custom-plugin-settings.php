<?php

/**
 * Add custom plugin settings. Usage example.
 * Result http://joxi.ru/l2ZLBaJcRKYkOr
 */

add_filter( 'es_get_available_settings', 'esc_get_available_settings' );

/**
 * Register custom plugin settings in settings container.
 *
 * @param $settings
 *
 * @return mixed
 */
function esc_get_available_settings( $settings ) {
	$settings['custom_option_1'] = array(
		'default_value' => 'My value'
	);

	$settings['custom_option_2'] = array(
		'values' => array(
			1 => 'One',
			2 => 'Two',
			3 => 'Three',
			4 => 'Four',
		),
		'default_value' => array( 1, 4 )
	);

	return $settings;
}

/**
 * Example of usage settings container functions.
 */
add_action( 'init', function() {
	if ( function_exists( 'ests_save_option' ) ) {
		ests_save_option( 'custom_option_1', "Modified value" );

		$value = ests( 'custom_option_1' ); // "Modified value".
		$value2 = ests( 'custom_option_2' ); // array(1,4).

		$allowed_values_of_custom_option_2 = ests_values( 'custom_option_2' ); // array(1,2,3,4)
		$default_option_value = ests_default( 'custom_option_1' ); // 'My value'
	}
} );

/**
 * Add custom tab on estatik settings page.
 *
 * @param $tabs
 */
function esc_settings_page_tabs( $tabs ) {
	$tabs['custom-tab'] = array(
		'label' => _x( 'Custom tab', 'plugin settings', 'esc' ),
		'template' => ESC_PLUGIN_PATH . '/admin/templates/settings/custom-tab.php',
	);

	return $tabs;
}
add_filter( 'es_settings_page_tabs', 'esc_settings_page_tabs' );
