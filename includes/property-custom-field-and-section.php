<?php

/**
 * Create property custom fields.
 *
 * @param $fields
 *
 * @return array
 */
function esc_property_default_fields( $fields ) {
	$fields['text_field'] = array(
		'label' => __( 'Text field', 'esc' ),
		'type' => 'text',
		'section_machine_name' => 'basic-facts', // Single property page section.
		'tab_machine_name' => 'location', // Admin property form creation tab.

		// FB field setting.
		'fb_settings' => array(
			'disable_name_edit' => false, // Disable edit field name in Field builder.
			'disable_type_edit' => true, // Disable field type field in Field builder.
		),
	);

	$fields['select_field_custom_section'] = array(
		'label' => __( 'Select field custom section', 'esc' ),
		'type' => 'select',
		'search_support' => true, // Allow this field in search form.
		'section_machine_name' => 'custom-section', // Single property page section.
		'tab_machine_name' => 'custom-section', // Admin property form creation tab.
		'options' => array(
			'val1' => __( 'Val1', 'esc' ),
			'val2' => __( 'Val2', 'esc' ),
		),
		'attributes' => array(
			'placeholder' => __( 'Select field placeholder', 'esc' ),
		),
		'is_visible' => true, // Field is visible on frontend.
	);

	return $fields;
}
add_filter( 'es_property_default_fields', 'esc_property_default_fields' );

/**
 * Add custom property section (tab & section).
 *
 * @param $sections
 *
 * @return mixed
 */
function esc_get_default_sections( $sections ) {
	$sections['property']['custom-section'] = array(
		'label' => __( 'Custom section', 'es' ),
		'order' => 100, // Section order.
		'is_visible' => true, // Visible section
		'is_visible_for' => array( 'all_users' ), // Visible for all users. Allowed values are all_users, admin,  agents, buyers, authenticated_users
	);

	return $sections;
}
add_filter( 'es_get_default_sections', 'esc_get_default_sections' );