<?php

add_action( 'init', function() {

	$query_args = es_get_properties_query_args( array(
		// Default wp args of wp_query object.
		'query' => array(
			'posts_per_page' => 5,
			'post_status' => 'publish',
		),
		// Custom fields query.
		'fields' => array(
			'sort' => 'newest', // Allowed values are newest, oldest, lowest_price, highest_price, largest_sq_ft, bedrooms, bathrooms and all es_label taxonomy terms slugs.
			'from_bedrooms' => 2,
			'from_bathrooms' => 2,
			'from_price' => 100,
			'min_area' => 50, // Also allowed from_area
			'keywords' => array( 'Property Title', 5, 'listing address here' ), // Where 5 is property ID.
			'es_category' => array( 1, 2, 3, 4, 5 ), // Category taxonomy terms IDs.
			'select_field_custom_section' => 'val1', // Before created custom field.
		),
	) );

	// Query args array for retrieve properties.
    // var_dump( $query_args );

	// Get properties.
	$posts = get_posts( $query_args );

	// WP_Query for properties.
	$query = new WP_Query( $query_args );
} );
