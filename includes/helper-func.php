<?php

add_action( 'init', function() {

	/**
	 * The first argument of es_format_value funtion is values that need to be formatted.
	 * The second argument is type of formatter (not field machine name).
	 *
	 * @see es_format_value to get list of allowed formatters.
	 */

	// Format field to price format.
	$formatted_price = es_format_value( 10000.45, 'price' );

	// Formatted date using human_time_diff func.
	$formatted_date = es_format_value( time(), 'date_added' );

	// Formatted beds, baths, floors, half_baths
	$formatted_beds = es_format_value( 1, 'beds' ); // 1 bed
	$formatted_baths = es_format_value( 3, 'baths' ); // 3 baths
	$formatted_floors = es_format_value( 4, 'floors' ); // 4 floors
	$formatted_half_baths = es_format_value( 5, 'half_baths' ); // 5 half baths

	// Where 100 is property ID. Return Link HTML.
	$formatted_post_link = es_format_value( 100, 'post-link' );

	$formatted_area = es_format_value( 5, 'area' ); // 5 sq ft
	$formatted_lot_size = es_format_value( 5, 'lot_size' ); // 5 m2

	// array( 1, 2, 3, 4 ) are document attachments ids. Return documents HTML.
	$formatted_documents = es_format_value( array( 1, 2, 3, 4 ), 'documents' );

	// array( 1, 2, 3, 4 ) are images attachments ids. Return images list HTML.
	$formatted_documents = es_format_value( array( 1, 2, 3, 4 ), 'images' );

	// Building has 5 sq ft of area
	$formatted_area = es_format_value( 5, 'area', array( 'pref' => 'Building has ', 'suff' => ' of area' ) );

	/**
	 * Plugin templates paths and template including.
	 */

	/**
	 * Return template path. If template is overridden then function return a path of overridden template.
	 */
	$property_item_path = es_locate_template( 'front/content-archive.php' );

	/**
	 * The same as es_locate_template but this function include template instead of return template path.
	 */
	// es_load_template( 'front/shortcodes/my-listings.php' );
} );
