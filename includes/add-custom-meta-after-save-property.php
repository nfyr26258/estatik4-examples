<?php

add_action( 'es_after_save_property', 'esc_after_save_property' );

/**
 * Fires after save property.
 *
 * @param $post_id
 */
function esc_after_save_property( $post_id ) {
	update_post_meta( $post_id, 'custom-action', 'fire' );
}
