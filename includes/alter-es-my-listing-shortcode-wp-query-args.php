<?php

/**
 * Alter query of [es_my_listing] shortcode.
 *
 * @param $query_args
 *
 * @return mixed
 */
function esc2_es_my_listing_query_args( $query_args ) {
	$your_custom_condition = false;

	if ( $your_custom_condition ) {
		$query_args['meta_query'][] = array(
			'key' => 'custom-action',
			'value' => 'fire',
		);
	}

	return $query_args;
}
add_filter( 'es_es_my_listing_query_args', 'esc2_es_my_listing_query_args' );
