<?php

/**
 * Create custom property taxonomy field and add on data manager page.
 * Results:
 * http://joxi.ru/LmGyxX6ugQjJgA
 * http://joxi.ru/KAgYpvlcN10KXm
 */

add_action( 'init', 'esc2_register_taxonomy' );

/**
 * Register taxonomy for custom custom-taxonomy-field field.
 *
 * @return void
 */
function esc2_register_taxonomy() {
	$args = array(
		'labels' => array(
			'name' => __( 'Custom taxonomy field', 'es' ),
			'singular_name' => __( 'Custom taxonomy field', 'es' ),
		),
	);

	register_taxonomy( 'custom-taxonomy-field', 'properties', $args );
}

add_filter( 'es_property_default_fields', 'esc2_property_default_fields' );

/**
 * Add property taxonomy custom field.
 *
 * @param $fields
 *
 * @return mixed
 */
function esc2_property_default_fields( $fields ) {
	$fields['custom-taxonomy-field'] = array(
		'label' => __( 'Custom taxonomy field', 'es' ),
		'search_support' => true,
		'type' => 'radio',
		'taxonomy' => true,
		'section_machine_name' => 'basic-facts',
	);

	return $fields;
}

/**
 * Add previously created taxonomy field on data manager page.
 *
 * @return void
 */
function esc_data_manager_add_custom_field() {
	echo "<div class='es-row'><div class='es-col-6'>";
	$creator = es_get_terms_creator_factory( 'custom-taxonomy-field' );
	$creator->render();
	echo "</div></div>";
}
add_action( 'es_data_manager_parameters_after', 'esc_data_manager_add_custom_field' );

/**
 * Add newly created taxonomy field to the sidebar nav.
 *
 * @param $tabs
 *
 * @return array
 */
function esc_date_manager_get_nav_items( $tabs ) {
	$tabs['parameters'][] = array(
		'label' => __( 'Custom taxonomy field', 'es' ),
		'hash' => '#es-terms-custom-taxonomy-field-creator', // #es-terms-%s-creator
	);

	return $tabs;
}
add_filter( 'es_date_manager_get_nav_items', 'esc_date_manager_get_nav_items' );
