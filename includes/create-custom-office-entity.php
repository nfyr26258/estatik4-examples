<?php

/**
 * Create custom plugin office entity. Entity usage example.
 */

if ( class_exists( 'Es_Post' ) ) {

	add_action( 'init', function() {
		register_post_type( 'office' );
	} );

	/**
	 * class Esc_Office
	 *
	 * @property $office_mls_id string
	 * @property $post_title string
	 * @property $agents_ids array
	 * @property $office_creation int
	 */
	class Esc_Office extends Es_Post {

		/**
		 * Postmeta key prefix.
		 *
		 * @return string
		 */
		public function get_entity_prefix() {
			return 'esc_office_';
		}

		/**
		 * Set custom office field.
		 *
		 * @return array[]
		 */
		public static function get_default_fields() {
			return array(
				'post_title' => array(
					'system' => true,
					'label' => __( 'Office name', 'esc' ),
				),
				'office_mls_id' => array(
					'label' => __( 'Office MLS', 'esc' ),
				),
				'agents_ids' => array(
					'label' => __( 'Agents', 'esc' ),
				),
				'office_creation' => array(
					'label' => __( 'Created at', 'esc' ),
				),
			);
		}

		/**
		 * Post type name.
		 *
		 * @return string
		 */
		public static function get_post_type_name() {
			return 'office';
		}
	}

	/**
	 * Integrate custom entity as estatik entity for property work of loop functions such as es_get_the_field
	 *
	 * @param $entity
	 * @param $post_id
	 * @param $wp_type
	 *
	 * @return mixed
	 */
	function esc_get_entity_by_id( $entity, $post_id, $wp_type ) {
		$post = get_post( $post_id );

		if ( $post && $post->post_type == 'office' && $wp_type == 'post' ) {
			$entity = esc_get_the_office( $post_id );
		}

		return $entity;
	}
	add_filter( 'es_get_entity_by_id', 'esc_get_entity_by_id', 10, 3 );

	/**
	 * @param int $post_id
	 *
	 * @return Esc_Office
	 */
	function esc_get_the_office( $post_id = 0 ) {
		$post = get_post( $post_id );
		return new Esc_Office( $post->ID );
	}

	add_action( 'init', function() {
		if ( get_post_type( 100 ) == 'office' ) {
			$office = esc_get_the_office( 100 ); // Where 100 is office post ID.

			// Save entity fields.
			$office->save_fields( array(
				'office_mls_id' => 'ML12345',
				'agents_ids' => array( 200, 201, 202 ), // Where array( 200, 201, 202 ) are list of agents IDs.
			) );

			// Save single entity field.
			$office->save_field_value( 'office_creation', time() );

			$mls_id = es_get_the_field( 'office_mls_id', 100 ); // Where office_mls_id is field machine name and 100 is post id.
			// OR
			$mls_id_2 = $office->office_mls_id;

			$office_name = $office->post_title;
		}
	} );
}
