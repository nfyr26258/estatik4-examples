<?php

/**
 * Estatik framework usage.
 *
 * See all of fields types in estatik-plugin/includes/classes/framework/class-field-factory.php
 */

add_action( 'init', function() {
	$my_condition = false;

	if ( $my_condition ) {

		/**
		 * Return field instance.
		 *
		 * @var Es_Framework_Field $field
		 */
		$field = es_framework_get_field( 'field_id_or_name', array(
			'type' => 'text',
			'value' => 'My field value'
		) );

		/**
		 * Display field immediately.
		 */
		es_framework_field_render( 'field_id_or_name_2', array(
			'type' => 'select',
			// Field html attributes.
			'attributes' => array(
				'name' => 'modified_field_name',
				'class' => 'js-es-field-class',
				'disabled' => 'disabled',
				'placeholder' => 'Placeholder needed?',
			),
			// Field values.
			'options' => array( array_combine( range( 1, 10 ), range( 1, 10 ) ) )
		) );

		/**
		 * Return field HTML.
		 */
		$field_html = es_framework_get_field_html( 'pwd', array(
			'type' => 'password',
		) );

		echo $field_html;
	}
} );
