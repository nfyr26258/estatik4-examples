<?php

/**
 * Add custom property & agent section.
 *
 * @param $sections
 *
 * @return mixed
 */
function esc_add_agent_and_property_section( $sections ) {
	$sections['property']['custom-section-custom-content'] = array(
		'label' => 'Custom section with custom content',
		'order' => 90,
		'is_visible' => true,
		'render_action' => 'my_property_custom_section_content'
	);

	$sections['agent']['custom-section-custom-content'] = array(
		'label' => 'Custom section with custom content',
		'order' => 90,
		'is_visible' => true,
		'render_action' => 'my_agent_custom_section_content'
	);

	return $sections;
}
add_filter( 'es_get_default_sections', 'esc_add_agent_and_property_section' );

add_action( 'my_property_custom_section_content', 'my_property_custom_section_content_callback' );

/**
 * Render property section on single page.
 * Result http://joxi.ru/a2XWpd8tlE8V0m
 */
function my_property_custom_section_content_callback() {
	printf( "Property section custom content. Bedrooms: %s", es_get_the_formatted_field( 'bedrooms' ) );
}

add_action( 'my_agent_custom_section_content', 'my_agent_custom_section_content_callback' );

/**
 * Render agent section in single page.
 * Result http://joxi.ru/brRe4VXsL5X4aA
 */
function my_agent_custom_section_content_callback() {
	printf( "Agent section custom content. position: %s", es_get_the_field( 'position' ) );
}