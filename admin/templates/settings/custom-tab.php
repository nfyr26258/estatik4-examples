<h2><?php _e( 'Custom settings tab', 'es' ); ?></h2>

<?php

/**
 * Create estatik settings custom tab.
 * Result http://joxi.ru/VrwYOgPcjpGpG2
 */

es_settings_field_render( 'custom_option_1', array(
	'label' => __( 'Custom option 1', 'esc' ),
	'type' => 'text',
	'attributes' => array(
		'placeholder' => __( 'Placeholder if needed', 'es' ),
	),
) );

es_settings_field_render( 'custom_option_2', array(
	'label' => __( 'Custom option 2', 'esc' ),
	'type' => 'checkboxes',
	'attributes' => array(
		'placeholder' => __( 'Choose option', 'es' ),
		'multiple' => 'multiple',
	),
) );
